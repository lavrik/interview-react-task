import React, {ReactNode} from 'react';
import './Dropdown.css';

interface DropdownProps {
	target: ReactNode;
	children: ReactNode;
}

export function Dropdown({target, children}: DropdownProps) {

	// TODO

	return (
		<div className='dropdown'>
			<button>{target}</button>
			<div className='dropdown__content'>{children}</div>
		</div>
	);
}
