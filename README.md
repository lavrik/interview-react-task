#### Реализовать компонент Dropdown

Компонент должен реализовывать стандартное поведение дропдауна - принимая в props-ах `target` и `content` (в данном случае `children`) элементы и обеспечивает логику оторажения или скрытия контента по клику на `target`.

Интерфейс props-ов:  
```
interface DropdownProps {
	target: ReactNode;
	children: ReactNode;
}
```

По умолчанию блок с контентом дропдауна отображается под `target` элементом. 
При этом, в момент отображения контента необходимо проверить, помещается ли блок в контентом во вьюпорт. Если нет, то необходимо отображать его над `target` элементом.


На скрине наглядно отображена данная логика:   
<img src="screen.png"  width="520">


----

Для начала работы необходимо клонировать репозиторий и

1. установить зависимости: `npm i`
1. запустить приложение (используется create-react-app) `npm start`
